const { Sequelize, DataTypes } = require('sequelize');
const { sequelize } = require('../db/db.config')

const fs = require('fs')
const csv = require('fast-csv')

const People = sequelize.define('People', {
  // Model attributes are defined here
  uuid: {
    type: DataTypes.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4
  },
  survived: {
    type: DataTypes.STRING,
  },
  passangerClass: {
    type: DataTypes.STRING,
  },
  name: {
    type: DataTypes.STRING,
  },
  sex: {
    type: DataTypes.STRING,
  },
  age: {
    type: DataTypes.STRING,
  },
  siblingsOrSpousesAboard: {
    type: DataTypes.STRING,
  },
  parentsOrChildrenAboard: {
    type: DataTypes.STRING,
  },
  fare: {
    type: DataTypes.STRING,
  },
});

(async () => {
  try {
    await People.sync({ alter: true });
    const people = [];
    fs.createReadStream(__dirname + '/../titanic.csv')
      .pipe(csv.parse({ headers: true }))
      .on('error', error => console.error(error))
      .on('data', row => people.push(row))
      .on('end', async () => {
        await People.bulkCreate(people).then(() => {
          console.log('Titanic people data seeded to the database');
        })
      });
  } catch (error) {
    console.log(error);
  }
})
// ();

module.exports = People;