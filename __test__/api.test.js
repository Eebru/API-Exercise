process.env.NODE_ENV = "test";
const app = require("../app");
const request = require("supertest");
const People = require("../models/people");

beforeAll(async () => {
	await People.sync({ alter: true });
	await People.create({
		survived: "true",
		passangerClass: "1",
		name: "Mr. Alao Opeyemi",
		sex: "Male",
		age: "30",
		siblingsOrSpousesAboard: "0",
		parentsOrChildrenAboard: "0",
		fare: "80",
	});
});

afterAll(async () => {
	await People.drop();
});

describe("1 GET all /people", () => {
	test("Returns all the people in the DB", async () => {
		const res = await request(app).get("/people");
		expect(res.body.success).toBe(true);
		expect(res.statusCode).toBe(200);
	});
});

describe("POST to /people", () => {
	test("Insert a person data into the database", async () => {
		const res = await request(app).post("/people").send({
			survived: "true",
			passangerClass: "1",
			name: "Mr. Alao Opeyemi",
			sex: "Male",
			age: "30",
			siblingsOrSpousesAboard: "0",
			parentsOrChildrenAboard: "0",
			fare: "80",
		});
		expect(res.body.success).toBe(true);
		expect(res.body.message).toHaveProperty("name");
		expect(res.body.message).toHaveProperty("uuid");
		expect(res.statusCode).toBe(200);

		//get the total number of people
		const res2 = await request(app).get("/people");
		expect(res2.body.message.data.length).toBe(2);
	});
});

describe("GET by uuid /people/uuid", () => {
	test("It should get a person by their UUID", async () => {
		const getPeople = await request(app).get("/people");

		const res = await request(app).get(
			`/people/${getPeople.body.message.data[0].uuid}`
		);

		expect(res.body.success).toBe(true);
		expect(res.body.message[0]).toHaveProperty("name");
		expect(res.body.message[0].uuid).toBe(getPeople.body.message.data[0].uuid);
		expect(res.statusCode).toBe(200);
	});
});

describe("PUT by uuid /people/uuid", () => {
	test("It should edit the person's data by their UUID", async () => {
		const getPeople = await request(app).get("/people");

		const res = await request(app)
			.put(`/people/${getPeople.body.message.data[0].uuid}`)
			.send({ name: "Mr. Eebru Alao" });

		expect(res.body.success).toBe(true);
		expect(res.body.message.data).toHaveProperty("name");
		expect(res.body.message.data.name).toBe("Mr. Eebru Alao");
		expect(res.body.message.data.uuid).toBe(getPeople.body.message.data[0].uuid);
		expect(res.statusCode).toBe(200);
	});
});

describe("DELETE by uuid /people/uuid", () => {
	test("It should delete a person by their UUID", async () => {
		const getPeople = await request(app).get("/people");

		const res = await request(app).delete(
			`/people/${getPeople.body.message.data[0].uuid}`
		);

		//get the total number of people
		const res2 = await request(app).get("/people");

    expect(res.body.message).toBe(`user with the ID ${getPeople.body.message.data[0].uuid} is deleted`);
		expect(res2.body.success).toBe(true);
		expect(res2.body.message.data.length).toBe(1);
	});
});