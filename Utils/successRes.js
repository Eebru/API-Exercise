module.exports = (statusCode, message, res) => {
  return res.status(statusCode).json({
    success: true,
    message
  });
};
