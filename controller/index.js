const db = require("../models/people");
const successRes = require("../Utils/successRes");
const errorRes = require("../Utils/errorRes");

module.exports = class Controller {
	async getPeople(req, res) {
		try {
			const people = await db.findAndCountAll();
			return successRes(
				200,
				{
					info: `${people.count} is the total number of people.`,
					data: people.rows,
				},
				res
			);
		} catch (error) {
			return errorRes(400, "Error fetching people data from the database", res);
		}
	}

	async addPeople(req, res) {
		try {
			const people = await db.create(req.body);
			return successRes(200, people, res);
		} catch (error) {
			console.log(error);
			return errorRes(400, "Error creating new people record", res);
		}
	}

	async convertSurviveToTrue(req, res) {
		try {
			await db.update(
				{ survived: "true" },
				{
					where: {
						survived: "1",
					},
				}
			);
			await db.update(
				{ survived: "false" },
				{
					where: {
						survived: "0",
					},
				}
			);
			return successRes(200, "Survive column was updated successfully", res);
		} catch (error) {
			console.log(error);
			return errorRes(400, "Unable to convert Survive column", res);
		}
	}

	async findByName(req, res) {
		try {
			if (!req.body.name) {
				return errorRes(400, `Please insert the name search parameter`, res);
			}
      const foundPerson = await db.findAll({
				where: {
					name: req.body.name,
				},
			});
			if (foundPerson.length < 1) {
				return errorRes(400, `Person with name ${req.body.name} is not found`, res);
			}
			const foundPerson2 = await db.findAll({
				where: {
					name: req.body.name,
				},
			});
			return successRes(200, foundPerson2, res);
		} catch (error) {
			return errorRes(400, "Error fetching", res);
		}
	}

	async getByUUID(req, res) {
		try {
			if (!req.params.id) {
				return errorRes(400, `UUID parameter is not found`, res);
			}
			const foundPerson = await db.findAll({
				where: {
					uuid: req.params.id,
				},
			});
			if (foundPerson.length < 1) {
				return errorRes(400, `Person with ID ${req.params.id} is not found`, res);
			}
			return successRes(200, foundPerson, res);
		} catch (error) {
			console.log(error);
			return errorRes(400, `Error fetching user with ID ${req.params.id}`, res);
		}
	}

	async getAllDeadPeople(req, res) {
		try {
			const foundPerson = await db.findAndCountAll({
				where: {
					survived: "false",
				},
			});
			return successRes(
				200,
				{
					info: `${foundPerson.count} people are total dead people`,
					data: foundPerson.rows,
				},
				res
			);
		} catch (error) {
			console.log(error);
			return errorRes(400, `Error fetching all dead people`, res);
		}
	}

	async getAllSurvivedPeople(req, res) {
		try {
			const foundPerson = await db.findAndCountAll({
				where: {
					survived: "true",
				},
			});
			return successRes(
				200,
				{
					info: `${foundPerson.count} people are total survived people`,
					data: foundPerson.rows,
				},
				res
			);
		} catch (error) {
			console.log(error);
			return errorRes(400, `Error fetching all survived people`, res);
		}
	}

	async deleteByUUID(req, res) {
		try {
			if (!req.params.id) {
				return errorRes(400, `UUID parameter is not found`, res);
			}
			const foundPerson = await db.findAll({
				where: {
					uuid: req.params.id,
				},
			});
			if (foundPerson.length < 1) {
				return errorRes(400, `Person with ID ${req.params.id} is not found`, res);
			}
			await db.destroy({
				where: {
					uuid: req.params.id,
				},
			});
			return successRes(
				200,
				`user with the ID ${req.params.id} is deleted`,
				res
			);
		} catch (error) {
			console.log(error);
			return errorRes(400, `Error deleting user with ID ${req.params.id}`, res);
		}
	}

	async patchByUUID(req, res) {
		try {
			if (!req.params.id) {
				return errorRes(400, `UUID parameter is not found`, res);
			}
			const foundPerson = await db.findAll({
				where: {
					uuid: req.params.id,
				},
			});
			if (foundPerson.length < 1) {
				return errorRes(400, `Person with ID ${req.params.id} is not found`, res);
			}
			await db.update(req.body, {
				where: {
					uuid: req.params.id,
				},
			});
			const foundPerson2 = await db.findOne({
				where: {
					uuid: req.params.id,
				},
			});
			return successRes(
				200,
				{
					info: `Person with ID ${req.params.id} has been updated succesfully`,
					data: foundPerson2,
				},
				res
			);
		} catch (error) {
			console.log(error);
			return errorRes(400, `Error Updating user with ID ${req.params.id}`, res);
		}
	}
};
