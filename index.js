const app = require('./app')
// The database instance instantiation
const { connectDB } = require('./db/db.config');

// For environment variables
require("dotenv").config();

const PORT = process.env.PORT || 7000;

// Connecting the database before starting the server.
connectDB().then(()=> {
  app.listen(PORT, () => {
    console.log(`Server started on port: ${PORT}...`);
  })
});

// Handle unhandled promise rejections
process.on("unhandledRejection", (err) => {
  console.log(`Error: ${err.message}`);
  // Close server & exit process
  app.close(() => process.exit(1));
});