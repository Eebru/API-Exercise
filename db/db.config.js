require('dotenv').config();
const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
  database: process.env.NODE_ENV === 'test' ? process.env.TEST_DB : process.env.DB, 
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_ENDPOINT,
  dialect: 'postgres',
  logging: false
})

async function connectDB() {
  try {
    await sequelize.authenticate();
    console.log("Database is Connected!");
  } catch (error) {
    console.error("Unable to connect to the database:", error.stack);
  }
}

module.exports = {connectDB, sequelize};