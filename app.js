const express = require('express');
const app = express()
const morgan = require("morgan");


// express body-parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json());

// morgan middleware for logging accesses
app.use(morgan("tiny"));

// The Routes
app.use('/people', require('./routes'));


module.exports = app