const routes = require('express').Router();
const Controller = require('../controller')
const theController = new Controller()


routes.post('/convert', theController.convertSurviveToTrue);

routes.post('/', theController.addPeople);

routes.get('/', theController.getPeople);

routes.post('/findpersonbyname', theController.findByName);

routes.get('/alldead', theController.getAllDeadPeople);

routes.get('/allsurvived', theController.getAllSurvivedPeople);

routes.get('/:id', theController.getByUUID);

routes.delete('/:id', theController.deleteByUUID);

routes.put('/:id', theController.patchByUUID);

module.exports = routes;